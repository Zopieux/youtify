#!/usr/bin/python
# coding: utf-8
from __future__ import unicode_literals, print_function

import argparse
import logging
import requests
import requests.exceptions

API_PLAYLIST_URL = 'https://www.googleapis.com/youtube/v3/playlists'
API_PLAYLIST_ITEMS_URL = 'https://www.googleapis.com/youtube/v3/playlistItems'
OPT_DEFAULTS = {'max_results': 50, 'api_key': '.youtify.apikey'}


def create_playlist_from_track(track):
    # TODO
    pass


def get_videos_information(api_key, playlist_id, max_results):
    logging.info('getting video information')
    payload = {'part': 'snippet',
               'playlistId': playlist_id,
               'key': api_key,
               'maxResults': max_results}
    r = requests.get(API_PLAYLIST_ITEMS_URL, params=payload)
    for item in r.json()['items']:
        yield tuple(item['snippet']['title'].split('-', 1))


def get_youtube_playlists(api_key, channel_id, max_results):
    logging.info('getting user playlists')
    payload = {'part': 'snippet',
               'channelId': channel_id,
               'key': api_key,
               'maxResults': max_results}
    try:
        r = requests.get(API_PLAYLIST_URL, params=payload)
        return r.json()['items']
    except requests.exceptions.Timeout as e:
        logging.error('Timeout when retrevieng playlist', e)
    except requests.exceptions.TooManyRedirects as e:
        logging.error('Redirect loop while retrevieng playlist', e)
    except requests.exceptions.RequestException as e:
        logging.error('Unknown error occured while retrieving playlist', e)


if __name__ == '__main__':
    # Command line parsing
    parser = argparse.ArgumentParser(description="Converts Youtube channels to Spotify playlists.")
    parser.add_argument('-a', '--api-key', metavar='API_KEY_FILE', default=OPT_DEFAULTS['api_key'],
                        help="Youtube API key storage file (default: {})".format(OPT_DEFAULTS['api_key']))
    parser.add_argument('channel', nargs='+', help="Youtube channels to retrieve playlists from")
    parser.add_argument('-n', '--max-results', default=OPT_DEFAULTS['max_results'], help="Set maximum results per page")
    parser.add_argument('-v', '--verbose', action='store_true', dest='verbose', help="Enable verbose mode")

    args = parser.parse_args()
    with open(args.api_key) as f:
        # May rightfully raise IOError if no such file
        args.api_key = f.read().strip()

    # Setup logging
    logging.basicConfig(level=logging.INFO if args.verbose else logging.WARNING,
                        format='%(asctime)s - %(levelname)s - %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')

    # Main
    playlists = get_youtube_playlists(args.api_key, args.channel, args.max_results)
    playlists.sort(key=lambda item: item['snippet']['title'].lower())

    print("Retrieved playlists are:")
    print()
    for i, pl in enumerate(playlists):
        print("{:>6d}. {}".format(i + 1, pl['snippet']['title']))

    print()
    ids = input("Playlists to import (eg. 1,2,5): ").split(',')
    ids = map(int, filter(lambda e: e.strip(), ids))  # remove empty. TODO: validation

    for id in ids:
        try:
            tracks = get_videos_information(args.api_key, playlists[id - 1]['id'], args.max_results)
            print(list(tracks))
        except IndexError as e:
            print("Playlist {} is invalid".format(id))
